import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import VegContainer from './components/VegContainer';
import store from './Redux/Store';


function App() {
  return (
    <Provider store={store}>
        <div className="App">
        <VegContainer />
      </div>
    </Provider>
    
  );
}

export default App;
