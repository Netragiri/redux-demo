import React from 'react'
import { connect } from 'react-redux'
import { buyVegetables, removeVegetables, resetVegetables } from '../Redux/VegActions'


function VegContainer(props) {
  return (
    <div>
      <h2>number of Vegetables :{props.quantity}</h2>
      <button onClick={props.buyvegetables}>Purchase</button>
      <button onClick={props.removevegetables}>Remove from cart</button>
      <button type='reset' onClick={props.resetvegetables}>Reset cart</button>
    </div>
  )
}

const mapStateToProps=state=>{
  return{
    quantity:state.quantity
  }
}

const mapDispatchToprops=dispatch=>{
  return{
    buyvegetables:()=>dispatch(buyVegetables()),
    removevegetables:()=>dispatch(removeVegetables()),
    resetvegetables:()=>dispatch(resetVegetables())
  }
}
export default connect(mapStateToProps,mapDispatchToprops)(VegContainer)
