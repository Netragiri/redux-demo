
import { Reset_Veg } from './VegConstants'
import { initialValues } from './VegReducers'

const RemoveReducer = (state = initialValues, action) => {
    console.log(initialValues, action?.type);
    switch (action?.type) {
        case Reset_Veg:
            return {
                ...state,
                quantity: initialValues.quantity
            }
        default: return state
    }
}

export default RemoveReducer
