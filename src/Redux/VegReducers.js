import { Buy_Veg, Remove_Veg, Reset_Veg } from "./VegConstants"

const initialValues = {
    quantity: 0
}

const VegReducer = (state = initialValues, action) => {
    console.log(action.type);
    switch (action.type) {
        case Buy_Veg:
            return {
                ...state,
                quantity: state.quantity + 1
            }
        case Remove_Veg:
            return {
                ...state,
                quantity: state.quantity - 1
            }
        case Reset_Veg:
            return{
                quantity:initialValues.quantity
            }
        default: return state
    }
}


export default VegReducer
export { initialValues }