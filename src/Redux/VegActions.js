import { Buy_Veg, Remove_Veg, Reset_Veg } from "./VegConstants"


const buyVegetables=()=>{
    return {
        type: Buy_Veg
    }
}
const removeVegetables=()=>{
    return {
        type: Remove_Veg
    }
}

const resetVegetables=()=>{
    return {
        type: Reset_Veg
    }
}
export {buyVegetables,removeVegetables,resetVegetables}