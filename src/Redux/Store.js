import { createStore } from "redux";
import RemoveReducer from "./RemoveReducer";
import VegReducer from "./VegReducers";

const store=createStore(
    VegReducer
)

export default store